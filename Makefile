build:
	docker compose build

up:
	docker compose up -d

down:
	docker compose stop

bash:
	docker compose run ruby /bin/sh
