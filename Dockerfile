FROM ruby:2.7

ENV APP /app
WORKDIR $APP

COPY Gemfile $APP
COPY Gemfile.lock $APP

RUN bundle install
