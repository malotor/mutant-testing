# spec/spec_helper.rb
require 'rspec'
require 'simplecov'

SimpleCov.start do
  enable_coverage :branch
end

RSpec.configure do |config|
  config.order            = 'random'
end
