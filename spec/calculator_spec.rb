require 'spec_helper'
require_relative '../calculator'

RSpec.describe Calculator do
  it 'sum two numbers' do
    calculator = Calculator.new

    expect(calculator.add(2, 2)).to eq(4)
  end

  it 'subtract two numbers' do
    calculator = Calculator.new

    expect(calculator.substract(2, 2)).to eq(0)
  end

  it 'multiply two numbers' do
    calculator = Calculator.new

    expect(calculator.multiply(2, 2)).to eq(4)
  end

  it 'divide two numbers' do
    calculator = Calculator.new

    expect(calculator.divide(4, 2)).to eq(2)
  end

  # it 'raise an error on division by 0' do
  #   calculator = Calculator.new

  #   expect{ calculator.divide(4, 0) }.to raise_error(ZeroDivisionError)
  # end
end
